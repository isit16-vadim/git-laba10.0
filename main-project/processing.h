#pragma once
#ifndef PROCESSING_H
#define PROCESSING_H

#include "marapon.h"

int process(marapon* array[], int size);

#endif
