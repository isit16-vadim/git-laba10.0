#include "filter.h"
#include <cstring>
#include <iostream>

marapon** filter(marapon* array[], int size, bool (*check)(marapon* element), int& result_size)
{
	marapon** result = new marapon * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_marapone_by_human(marapon* element)
{
	return strcmp(element->human.first_name, "") == 0 &&
		strcmp(element->human.middle_name, "") == 0 &&
		strcmp(element->human.last_name, "") == 0;
}

bool check_book_subscription_by_date(marapon* element)
{
	return element->date.starth == 12 && element->date.startm== 0;
}

